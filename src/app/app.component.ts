import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';

import { AuthService } from './views/auth/services/auth.service';
import { AppRouterUrls } from './app-routing.config';
import { NavigationService } from './shared/services/navigation.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  @ViewChild('sidenav') sidenav;

  userIsAuthenticated = false;
  private authListenerSubs: Subscription;
  constructor(
    private authService: AuthService,
    private navService: NavigationService,
    private router: Router
    ) { }
  appRouterUrls = AppRouterUrls;

  ngOnInit() {
    this.authService.autoAuthUser();
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authListenerSubs = this.authService
      .getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });

    this.navService.sidebarnav = this.sidenav;

    this.router.events.subscribe((val) => {
      if(val){
        this.sidenav.close();
      }
    });
  }


  onLogout() {
    this.authService.logout();
  }

  ngOnDestroy() {
    this.authListenerSubs.unsubscribe();
  }

}
