import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from "@angular/router";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { AuthService } from "../views/auth/services/auth.service";
import { AppRouterUrls } from '../app-routing.config';

@Injectable()
export class IsAuthenticatedGuard implements CanActivate {
  appRouterUrls = AppRouterUrls;

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    const isAuth = this.authService.getIsAuth();
    if (!isAuth) {
      this.router.navigate([this.appRouterUrls.LOGIN]);
    }
    return isAuth;
  }
}
