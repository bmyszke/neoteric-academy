import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from "rxjs";

import { AuthService } from 'src/app/views/auth/services';
import { AppRouterUrls } from '../../../app-routing.config';
import { NavigationService } from '../../services/navigation.service';

@Component({
  selector: 'app-navigation-menu',
  templateUrl: './navigation-menu.component.html',
  styleUrls: ['./navigation-menu.component.scss'],
})
export class NavigationMenuComponent implements OnInit, OnDestroy {

  appRouterUrls = AppRouterUrls;
  userIsAuthenticated = false;
  private authListenerSubs: Subscription;

  constructor(private authService: AuthService, private navService: NavigationService) {}

  ngOnInit() {
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authListenerSubs = this.authService
      .getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });

  }

  onLogout() {
    this.authService.logout();
  }

  toggleNav(){
    this.navService.sidebarnav.toggle();
  }
  ngOnDestroy() {
    this.authListenerSubs.unsubscribe();
  }
}
