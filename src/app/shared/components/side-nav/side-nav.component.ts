import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppRouterUrls } from 'src/app/app-routing.config';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/views/auth/services/auth.service';
import { NavigationService } from '../../services';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

  private authListenerSubs: Subscription;
  private navOpenerSubs:  Subscription;

  userName = "";
  private authListenerUsername : Subscription;

  userIsAuthenticated = false;
  isNavOpen = false;
  navStatus = 'close';
  appRouterUrls = AppRouterUrls;

  constructor(private authService: AuthService, private navService: NavigationService) {}

  ngOnInit() {
    this.authService.autoAuthUser();
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authListenerSubs = this.authService
      .getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });

    this.userName = this.authService.getUserName();
    this.authListenerUsername = this.authService.getAuthUserName().subscribe((nameOfUser) => {
      this.userName = nameOfUser;
    });
  }

  onLogout() {
    this.authService.logout();
  }
  ngOnDestroy(): void {
    this.authListenerUsername.unsubscribe();

  }
}
