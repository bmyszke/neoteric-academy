import { NavigationMenuComponent } from './navigation-menu/navigation-menu.component';
import { BrandHeaderComponent } from './brand-header/brand-header.component';
import { FiltersComponent } from './filters/filters.component';
import { DropdownDirective } from '../dropdown-directive/dropdown.directive';
import { SideNavComponent } from './side-nav/side-nav.component';


export const SharedComponents = [
  NavigationMenuComponent,
  BrandHeaderComponent,
  FiltersComponent,
  DropdownDirective,
  SideNavComponent
];

export {
  NavigationMenuComponent,
  BrandHeaderComponent,
  FiltersComponent,
  SideNavComponent
};
