import { Component, OnInit } from '@angular/core';
import { AppRouterUrls } from 'src/app/app-routing.config';

@Component({
  selector: 'app-brand-header',
  templateUrl: './brand-header.component.html',
  styleUrls: ['./brand-header.component.scss']
})
export class BrandHeaderComponent implements OnInit {
  appRouterUrls = AppRouterUrls;
  constructor() { }

  ngOnInit() {
  }

}
