import { FiltersService } from './filters.service';
import { NavigationService } from './navigation.service';


export const SharedServices = [
  FiltersService,
  NavigationService
];

export {
  FiltersService,
  NavigationService
};
