import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { OffersService } from '.';
import { Observable } from 'rxjs';
import { Offer } from '../offers.interface';

@Injectable({
  providedIn: 'root'
})
export class FetchOffersResolverService implements Resolve<Offer[]> {

constructor(private offersService: OffersService) { }


  resolve(route: ActivatedRouteSnapshot): Observable<Offer[]> {
    return this.offersService.fetchOffers();
  }
}
