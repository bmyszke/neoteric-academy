import { OffersComponent } from './offers/offers.component';
import { OffersListComponent } from './offers/offers-list/offers-list.component';
import { OfferSingleComponent } from './offers/offers-list/offer-single/offer-single.component';
import { OffersItemComponent } from './offers/offers-list/offers-item/offers-item.component';
import { MapsComponent } from './offers/maps/maps.component';
import { SalaryPipe } from 'src/app/shared/salary-pipe/salary.pipe';




export const OffersComponents = [
    OffersComponent,
    OffersListComponent,
    OfferSingleComponent,
    OffersItemComponent,
    MapsComponent,
    SalaryPipe
  ];
  
  export {
    OffersComponent,
    OffersListComponent,
    OfferSingleComponent,
    OffersItemComponent,
    MapsComponent,
    SalaryPipe
  };
  