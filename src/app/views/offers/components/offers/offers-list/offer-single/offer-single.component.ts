import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { OffersService } from 'src/app/views/offers/services/offers.service';
import { Offer } from '../../../../offers.interface';

@Component({
  selector: 'app-offer-single',
  templateUrl: './offer-single.component.html',
  styleUrls: ['./offer-single.component.scss']
})
export class OfferSingleComponent implements OnInit {
  offer: Offer;

  constructor(
    private offersService: OffersService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const url = this.route.snapshot.params['single-offer'];
    this.offer = this.offersService.getOfferbyUrl(url);
    this.offersService.currentCoordinates.next(this.offer.coordinates);
  }



}
