import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';

// import { Offer } from '../offer.model';
import { FiltersService } from '../../../../../shared/services';
import { OffersService } from '../../../services';
import { FiltersInterface, FiltersUrlInterface } from 'src/app/shared/services/filters.service';
import { Offer } from '../../../offers.interface';

@Component({
  selector: 'app-offers-list',
  templateUrl: './offers-list.component.html',
  styleUrls: ['./offers-list.component.scss']
})
export class OffersListComponent implements OnInit, OnDestroy {
  offers: Offer[];
  filters: FiltersInterface = this.filterService.getFilters();
  fromRouteParameters: Object;
  urls: FiltersUrlInterface = this.filterService.getUrls();
  place: string;
  technology: string;
  level: string;
  filterSubscription: Subscription;
  offerparams:Subscription;
  isLoading = false;
  private offerSub: Subscription;
  private ifOfferSub = false;

  constructor(
    private offerService: OffersService,
    private filterService: FiltersService,
    private route: ActivatedRoute,
    private router: Router) {


  }

  private setDefaultFilters(){
    //ustawiamy brakujące filtry na domyślne wartości
    this.fromRouteParameters =  {...this.route.snapshot.params};
    Object.keys(this.urls).map((key) => {
      if (this.fromRouteParameters.hasOwnProperty(key)) {
        this.urls[key] = this.fromRouteParameters[key];
      }
    });
    //ustawiamy filtry
    this.filterService.updateFiltersbyUrls(this.urls);
    this.offers = this.offerService.filterOffers(this.filters);
  }

  ngOnInit() {



    this.offers = this.offerService.getOffers();
    this.setDefaultFilters();
    this.isLoading = false;

    //loading when filterService changed
    this.filterSubscription = this.filterService.filtersChanged.subscribe(
      (newFilters: FiltersInterface) => {
        this.filters = newFilters;
        this.offers = this.offerService.filterOffers(newFilters);
      }
    );

    this.offerparams = this.route.params
      .subscribe(
        (params: Params) => {
          this.offerService.paramsFromOffer.next(params);
        }
      );

  }

  ngOnDestroy() {
    this.filterSubscription.unsubscribe();
    this.offerparams.unsubscribe();
    // if(this.ifOfferSub ){
    //  this.offerSub.unsubscribe();
    // }
  }


}
