import { Component, OnInit, OnDestroy } from '@angular/core';

import { latLng, marker, tileLayer, Map } from 'leaflet';
import { OffersService } from '../../../services';
import { Offer } from '../../../offers.interface';
import { FiltersService } from 'src/app/shared/services';
import { TechFilter, FiltersInterface, FiltersUrlInterface, Place } from 'src/app/shared/services/filters.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';


declare let L;

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit, OnDestroy {
  mapa: Map;
  mapaReady: boolean = false;
  zoom = 6;
  mapcenter = latLng([52.234982, 21.008490]);
  filterSubscription: Subscription;
  paramsSubscription: Subscription;
  paramsPatch: string;
  filters: FiltersInterface = this.filterService.getFilters();
  urls: FiltersUrlInterface = this.filterService.getUrls();
  place: Place;

  // Define our base layers so we can reference them multiple times
  streetMaps = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    detectRetina: true,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });

  MapIcon = L.Icon.extend({
    options: {
      shadowUrl: '',
      iconSize: [40, 40],
      shadowSize: [50, 64],
      iconAnchor: [40, 40],
      shadowAnchor: [4, 62],
      popupAnchor: [-20, -40]
    }
  });

  // An array of layers for markers
  markers = [];

  // Set the initial set of displayed layers (we could also use the leafletLayers input binding for this)
  options = {
    layers: [this.streetMaps],
    zoom: this.zoom
  };



  constructor(private offerService: OffersService, private filterService: FiltersService, private router: Router, private route: ActivatedRoute) {  }

  offers: Offer[] = this.offerService.getOffers();
  technologies: TechFilter[] = this.filterService.getTechs();

  generateIcons() {
    let icons = {};
    for (let filter of this.technologies) {
      icons[filter.name] = new this.MapIcon({ iconUrl: filter.icon });
    }
    return icons;

  }

  generateMarkers(offers: Offer[]) {
    let icons = this.generateIcons();

    for (let offer of offers) {
      let offerIcon = icons[offer.technologies];
      let offermarker = marker([offer.coordinates[0], offer.coordinates[1]], {
        icon: offerIcon,
        title: offer.position,
        bubblingMouseEvents: true
      });
      this.markers.push(offermarker);
      offermarker.bindPopup("<b>" + offer.position + "</b><br><span class='green'>" + offer.salaryMin + "000-" + offer.salaryMax + "000</span>");
    }
  }


  onMapReady(map: Map) {
    this.mapa = map;
    this.mapaReady = true;
  }

  setMapView( currentPlace : Place){
    if (currentPlace.city == "All" ||  currentPlace.city == "Remote"){
      this.zoom = 6
      this.mapcenter = latLng(currentPlace.coordinates[0], currentPlace.coordinates[1]);
    }
    else{
      this.zoom = 11;
      this.mapcenter = latLng(currentPlace.coordinates[0], currentPlace.coordinates[1]);
    }
  }

  ngOnInit() {
    this.offers = this.offerService.filterOffers(this.filters);
    this.generateMarkers(this.offers);
    this.place = this.filterService.getPlaces().find((el) => { return el.city === this.filters.place });
    this.setMapView(this.place);

    this.paramsPatch = this.route.snapshot.routeConfig.path;
    if (this.paramsPatch ==="offers") {
      this.paramsSubscription = this.offerService.currentCoordinates.subscribe((coord) => {
        this.zoom = 13
        this.mapcenter = latLng(coord[0], coord[1]);
      });
    }

    this.filterSubscription = this.filterService.filtersChanged.subscribe(
      (newFilters: FiltersInterface) => {
        this.offers = this.offerService.filterOffers(newFilters);
        this.markers = [];
        this.generateMarkers(this.offers);
        this.place = this.filterService.getPlaces().find((el) => { return el.city === newFilters.place });
        this.setMapView(this.place);
      }
    );

  }

  ngOnDestroy(): void {
    if (this.paramsPatch ==="offers") {
     this.paramsSubscription.unsubscribe();
    }
  }

}
