import { Component, OnInit } from '@angular/core';
import { OffersService } from '../../services/offers.service';
import { ActivatedRoute } from '@angular/router';
import { Offer } from '../../offers.interface';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit {

  constructor(private offerService: OffersService, private route: ActivatedRoute) { }

  fetchedOffers: Offer[] = [];
  ngOnInit() {

    this.fetchedOffers = this.route.snapshot.data.fetchedOffers;

    this.offerService.setOffers(this.fetchedOffers);
  }

}
