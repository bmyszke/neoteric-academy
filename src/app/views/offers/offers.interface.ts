export interface Offer {
  id: string;
  position: string;
  salaryMin: number;
  salaryMax: number;
  company: string;
  size: number;
  place: {
    city: string;
    street: string;
    houseNumber: string;
  };
  technologies: string;
  levels: string[];
  url: string;
  coordinates: number[];
  logo: string;
  emptype: string;
  shortdesc: string;
  skills: {
    name: string;
    leveltitle: string;
    levelnumber: number;
  }[],
  description: string;
  creator?: string;
}
