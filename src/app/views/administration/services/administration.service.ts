import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AppRouterUrls } from '../../../app-routing.config';

import { Offer } from '../../offers/offers.interface';
import { environment } from '../../../../environments/environment';



@Injectable({ providedIn: 'root' })
export class AdministrationService {
dbAddress = environment.apiUrl;

  private offers: Offer[] = [];
  private offersUpdated = new Subject<Offer[]>();

  constructor(private http: HttpClient, private router : Router) {}

  getOffers() {
    this.http
      .get<{ message: string; offers: any }>(
        this.dbAddress + '/offers'
      )
      .pipe(map((offerData) => {
        return offerData.offers.map(offer => {
          return {
            ...offer,
            id: offer._id
          };
        });
      }))
      .subscribe(transformedOffers => {
        this.offers = transformedOffers;
        this.offersUpdated.next([...this.offers]);
      });
  }

  getOffersUpdateListener() {
    return this.offersUpdated.asObservable();
  }

  addOffer(offerObj: Offer) {
    const offer = this.jsonToFormData(offerObj);
    this.http
      .post<{ message: string, offerId: string }>(this.dbAddress + '/offers', offer)
      .subscribe(responseData => {
        this.router.navigate([AppRouterUrls.ADMIN]);
      });
  }

  getOffer(id: string) {

    return this.http.get<Offer>(
      this.dbAddress + '/offers/' + id
    );
  }


  updateOffer(offerObj: Offer){
    let offerData: Offer | FormData;
    if (typeof offerObj.logo === "object") {
      offerData = this.jsonToFormData(offerObj);
    } else {
      offerData = offerObj;
    }
    this.http
      .put(this.dbAddress + '/offers/' + offerObj.id, offerData)
      .subscribe(response => {
        this.router.navigate([AppRouterUrls.ADMIN]);
      },
      error => {
        // console.log('error', error);
      });
  }

  deleteOffer(offerId: string) {
    this.http.delete(this.dbAddress + '/offers/' + offerId)
      .subscribe(() => {
        const updatedOffers = this.offers.filter(offer => offer.id !== offerId);
        this.offers = updatedOffers;
        this.offersUpdated.next([...this.offers]);
      });
  }


  buildFormData(formData, data, parentKey?) {
    if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
      Object.keys(data).forEach(key => {
        this.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
      });
    } else if (data instanceof File){
      const value = data == null ? '' : data;
      const filename = data.name.replace(/\.[^/.]+$/, "")

      formData.append(parentKey, value, filename);
    } else {
      const value = data == null ? '' : data;

      formData.append(parentKey, value);
    }
  }

  jsonToFormData(data) {
    const formData = new FormData();

    this.buildFormData(formData, data);

    return formData;
  }
}
