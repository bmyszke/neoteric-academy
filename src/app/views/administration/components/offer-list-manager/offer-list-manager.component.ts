import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from 'rxjs';

import { AdministrationService } from '../../services/administration.service';
import { AppRouterUrls } from '../../../../app-routing.config';
import { Offer } from '../../../offers/offers.interface';
import { AuthService } from 'src/app/views/auth/services/auth.service';


@Component({
  selector: 'app-offer-list-manager',
  templateUrl: './offer-list-manager.component.html',
  styleUrls: ['./offer-list-manager.component.scss']
})
export class OfferListManagerComponent implements OnInit, OnDestroy {
  appRouterUrls = AppRouterUrls;
  isLoading = false;
  offers: Offer[] = [];
  userIsAuthenticated = false;
  userId: string;
  private offerSub: Subscription;
  private authStatusSub: Subscription;

  constructor(public adminService: AdministrationService, private authService: AuthService) { }

  ngOnInit() {
    this.isLoading = true;
    this.adminService.getOffers();
    this.userId = this.authService.getUserId();
    this.offerSub = this.adminService.getOffersUpdateListener()
      .subscribe((offers: Offer[]) => {
        this.offers = offers;
        this.isLoading = false;
      });
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
        this.userId = this.authService.getUserId();
      });
  }

  onDelete(offerId: string) {
    this.adminService.deleteOffer(offerId);
  }

  ngOnDestroy() {
    this.offerSub.unsubscribe();
    this.authStatusSub.unsubscribe();
  }
}
