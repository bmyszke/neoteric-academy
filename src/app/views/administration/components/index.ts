import { OfferListManagerComponent } from './offer-list-manager/offer-list-manager.component';
import { OfferManagerComponent } from './offer-manager/offer-manager.component';


export const AdministrationComponents = [
  OfferListManagerComponent,
  OfferManagerComponent
];

export {
  OfferListManagerComponent,
  OfferManagerComponent
};
