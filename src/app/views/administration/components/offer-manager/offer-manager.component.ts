import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormArray, FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { EsriProvider } from 'leaflet-geosearch';
import { Subscription } from 'rxjs';

import { AdministrationService } from '../../services/administration.service';
import { Offer } from '../../../offers/offers.interface';
import { FiltersService } from 'src/app/shared/services';
import { LevelFilter, Place } from 'src/app/shared/services/filters.service';
import { mimeType } from "./mime-type.validator";
import { AuthService } from 'src/app/views/auth/services';


@Component({
  selector: 'app-offer-manager',
  templateUrl: './offer-manager.component.html',
  styleUrls: ['./offer-manager.component.scss']
})
export class OfferManagerComponent implements OnInit, OnDestroy {


  offer: Offer;
  offerForm: FormGroup;
  isLoading = false;
  imagePreview: string;
  private mode = "create";
  private offerId: string;
  authStatusSub: Subscription;

  levels: LevelFilter[] = this.filterService.getLevels();
  technologies: LevelFilter[] = this.filterService.getTechs();
  places: Place[] = this.filterService.getPlaces();
  skillvalues: string[] = ["1", "2", "3", "4", "5"];


  constructor(
    public adminService: AdministrationService,
    public route: ActivatedRoute,
    public filterService: FiltersService,
    public authService: AuthService
  ) { }

  ngOnInit() {
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe(authStatus => {
        this.isLoading = false;
      });


    for (var i = 0; i < this.levels.length; i++) {
      if (this.levels[i].name === 'All') {
        this.levels.splice(i, 1);
      }
    }
    for (var i = 0; i < this.technologies.length; i++) {
      if (this.technologies[i].name === 'All') {
        this.technologies.splice(i, 1);
      }
    }
    for (var i = 0; i < this.places.length; i++) {
      if (this.places[i].city === 'All') {
        this.places.splice(i, 1);
      }
    }

    this.offerForm = new FormGroup({
      position: new FormControl(null, { validators: [Validators.required] }),
      salaryMin: new FormControl(null, { validators: [Validators.required] }),
      salaryMax: new FormControl(null, { validators: [Validators.required] }),
      company: new FormControl(null, { validators: [Validators.required] }),
      size: new FormControl(null, { validators: [Validators.required] }),
      // place: new FormControl(null, { validators: [Validators.required] }),
      place: new FormGroup({
        city: new FormControl(null, { validators: [Validators.required] }),
        street: new FormControl(null, { validators: [Validators.required] }),
        houseNumber: new FormControl(null, { validators: [Validators.required] }),
      }),
      technologies: new FormControl(null, { validators: [Validators.required] }),
      levels: new FormControl(null, { validators: [Validators.required] }),
      url: new FormControl({ value: null, disabled: true }, { validators: [Validators.required] }),
      coordinates: new FormControl(null, { validators: [Validators.required] }),
      logo: new FormControl(null, {
        validators: [Validators.required],
        asyncValidators: [mimeType]
      }),
      emptype: new FormControl(null, { validators: [Validators.required] }),
      shortdesc: new FormControl(null, { validators: [Validators.required] }),
      skills: new FormArray([
        new FormGroup({
          name: new FormControl(null, [Validators.required]),
          leveltitle: new FormControl(null, [Validators.required]),
          levelnumber: new FormControl(1, [Validators.required])
        })
      ]),
      description: new FormControl(null, { validators: [Validators.required] })
    });

    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has("offerId")) {
        this.mode = "edit";
        this.offerId = paramMap.get("offerId");
        this.isLoading = true;
        this.adminService.getOffer(this.offerId).subscribe(offerData => {
          this.isLoading = false;

          for (let i = 0; i < offerData.skills.length - 1; i++) {
            (<FormArray>this.offerForm.get('skills')).push(
              new FormGroup({
                name: new FormControl(null, [Validators.required]),
                leveltitle: new FormControl(null, [Validators.required]),
                levelnumber: new FormControl(null, [Validators.required])
              })
            );
          }
          this.imagePreview = offerData.logo;
          this.offerForm.patchValue(offerData);
        });

      } else {
        this.mode = "create";
        this.offerId = null;
      }
    });


  }


  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.offerForm.patchValue({ logo: file });
    this.offerForm.get("logo").updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = <string>reader.result;
    };
    reader.readAsDataURL(file);
  }

  onSaveOffer() {
    if (this.offerForm.invalid) {
      return;
    }

    this.isLoading = true;
    if (!(this.offerForm.value.coordinates instanceof Array)) {
      this.offerForm.value.coordinates = this.offerForm.value.coordinates.split(",");
    }

    if (this.mode === "create") {
      this.adminService.addOffer(this.offerForm.getRawValue());
    } else {
      this.adminService.updateOffer({ ...this.offerForm.getRawValue(), id: this.offerId });
    }
    this.offerForm.reset();
  }

  onAddSkill() {
    const control = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      leveltitle: new FormControl(null, [Validators.required]),
      levelnumber: new FormControl(null, [Validators.required])
    });

    (<FormArray>this.offerForm.get('skills')).push(control);
  }
  onDeleteSkill(index: number) {
    (<FormArray>this.offerForm.get('skills')).removeAt(index);
  }

  onLoadSampleData() {
    const control = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      leveltitle: new FormControl(null, [Validators.required]),
      levelnumber: new FormControl(null, [Validators.required])
    });

    (<FormArray>this.offerForm.get('skills')).push(
      new FormGroup({
        name: new FormControl(null, [Validators.required]),
        leveltitle: new FormControl(null, [Validators.required]),
        levelnumber: new FormControl(null, [Validators.required])
      })
    );
    (<FormArray>this.offerForm.get('skills')).push(
      new FormGroup({
        name: new FormControl(null, [Validators.required]),
        leveltitle: new FormControl(null, [Validators.required]),
        levelnumber: new FormControl(null, [Validators.required])
      })
    );
    (<FormArray>this.offerForm.get('skills')).push(
      new FormGroup({
        name: new FormControl(null, [Validators.required]),
        leveltitle: new FormControl(null, [Validators.required]),
        levelnumber: new FormControl(null, [Validators.required])
      })
    );
    (<FormArray>this.offerForm.get('skills')).push(
      new FormGroup({
        name: new FormControl(null, [Validators.required]),
        leveltitle: new FormControl(null, [Validators.required]),
        levelnumber: new FormControl(null, [Validators.required])
      })
    );



    this.offerForm.setValue({
      position: 'Front End Developer',
      salaryMin: 5,
      salaryMax: 12,
      company: 'Netguru',
      size: 500,
      place: {
        city: "Gdańsk",
        street: "Grunwaldzka",
        houseNumber: "472E"
      },
      technologies: "HTML",
      levels: [
        "Junior", "Mid"
      ],
      url: "pierwsza-oferta",
      coordinates: [54.386140, 18.646010],
      logo: "invalid",
      emptype: "B2B",
      shortdesc: "Lorem ipsum dolor sit amet, consectetur a aliqua. Ut enim ad minim veniam, quis nostrud exdo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      skills: [
        {
          name: "Linux/Unix",
          leveltitle: "regular",
          levelnumber: 3
        },
        {
          name: "Docker/containers",
          leveltitle: "senior",
          levelnumber: 5,
        },
        {
          name: "Jenkins/CI",
          leveltitle: "mid",
          levelnumber: 3,
        },
        {
          name: "Chef/Ansible/Puppet",
          leveltitle: "regular",
          levelnumber: 3,
        },
        {
          name: "Cloud Computing",
          leveltitle: "junior",
          levelnumber: 1,
        }
      ],
      description: `
      <div>We are Netguru, one of the<a
          href="https://www.netguru.co/blog/netguru-ft1000-list-spotify-skyscanner?utm_campaign=Marketing%20-%20FT1000&amp;utm_content=52384070&amp;utm_medium=social&amp;utm_source=linkedin"
          target="_blank"> fastest growing companies in Europe</a>. We maintain an unshakable passion for developing
        web and mobile applications for our clients worldwide and making a name for ourselves in the world of
        software development.<br><br></div>
      <div>We have built an outstanding organisational culture based on transparency, team spirit, regular feedback,
        and continuous learning. Currently, we’re on the lookout for a passionate <strong>Devops Engineer</strong>
        who could join our team. If you have the talent and the skills it takes, apply NOW!<strong><br></strong><br>
      </div>
      <div><strong>Joining Netguru means:&nbsp;</strong></div>
      <ul>
        <li>working with an <strong>experienced and open-minded team of software specialists</strong>,</li>
        <li>ability to make an impact on the <strong>technological stack</strong> of the whole company,</li>
        <li><strong>constant development</strong> of your hard and soft skills (e.g. internal webinars or
          international and local conferences),</li>
        <li>Learning and polishing the skills which will make a <strong>perfect foundation for a future promotion
            and pay rise at Netguru</strong>,</li>
        <li><strong>flexplace</strong>: work from just about any place in the world and whenever suits you best.
        </li>
      </ul>
      <div><strong><br>Your responsibilities:</strong></div>
      <ul>
        <li>fixing<strong> day-to-day DevOps-related issues</strong> and helping the rest of Dev team deliver and
          ship their work,</li>
        <li><strong>configuring </strong>and<strong> automating processes</strong> related to building and
          maintaining development environments (<strong>using Ansible and Docker</strong>),</li>
        <li>implementing and maintaining monitoring,<strong> Continuous Integration </strong>and<strong>
            log-management systems,</strong></li>
        <li>keeping systems <strong>secure </strong>and<strong> up-to-date,</strong></li>
        <li><strong>creating and reviewing DevOps processes</strong>: e.g. backups and security checks,</li>
        <li><strong>automating</strong> day-to-day tasks.</li>
      </ul>
      <div><strong><br>Must-haves:</strong></div>
      <ul>
        <li>proven wide knowledge of the <strong>Unix environment,</strong></li>
        <li>experience with <strong>Docker </strong>and<strong> at least one orchestration tool</strong>:
          Kubernetes, Swarm, Mesos Marathon,</li>
        <li>experience with <strong>IT automated tools</strong> (like Ansible/Chef),</li>
        <li>usage of <strong>VCS on a daily basis</strong> (Git preferable),</li>
        <li>familiarity with <strong>CI tools</strong> (like Jenkins, CircleCI) and <strong>deployment
            pipelines,</strong></li>
        <li>knowledge of <strong>scripting languages</strong> (Bash/Ruby/Python),</li>
        <li><strong>basics understanding of network management</strong> (DNS, iptables),</li>
        <li>good command of <strong>written and spoken English and Polish (CEFR B2+),</strong></li>
        <li>ability to <strong>work independently</strong>,</li>
        <li>understanding and some experience (or a certificate) with<strong> one of the following cloud
            providers:</strong>
          <ul>
            <li>AWS Amazon services,</li>
            <li>Microsoft Azure,</li>
            <li>Google Cloud Platform.</li>
          </ul>
        </li>
      </ul>
      <div><strong><br>Nice-to-haves:</strong></div>
      <ul>
        <li>knowledge of <strong>load balancing/replication systems,</strong></li>
        <li>familiarity with <strong>NGINX or HAProxy,</strong></li>
        <li>some <strong>web development experience</strong> (super-happy if you know Rails or Node),</li>
        <li>track record of <strong>successful cooperation with developers.</strong></li>
      </ul>
      <div><strong><br>Perks &amp; Benefits:</strong></div>
      <ul>
        <li><strong>MacBook</strong> — we like to work with the best equipment,</li>
        <li>additional <strong>accessories</strong> for your computer,</li>
        <li><strong>private health insurance + MultiSport card,</strong></li>
        <li>individual<strong> co-financed educational plan,</strong></li>
        <li><strong>free sandwiches </strong>and<strong> half-price lunches,</strong></li>
        <li><strong>Team Retreats</strong> and <strong>Team meet-ups,</strong></li>
        <li>If you want to read more about it check our <a href="https://www.netguru.co/blog/reasons-work-netguru"
            target="_blank"><strong>7 reasons to work at Netguru</strong></a>.</li>
      </ul>
      <div><br><strong>We are just getting started 🚀 Ready to face the challenge?</strong></div>
      <div><strong>Don't hold off any longer and apply via the form!</strong></div>
    `,
    });

    this.offerForm.get("logo").updateValueAndValidity();
  }


  provider = new EsriProvider();
  address = '';
  options = [];
  lat: number = null;
  lng: number = null;

  onTypeCoordinates(event: any) {
    this.provider.search({ query: event.target.value }).then((resultData) => {
      this.options = resultData;
    });
  }


  onTypePosition(event: any) {
    const url = event.target.value
      .toLowerCase()
      .split(" ")
      .join("-")
      .split(".")
      .join("-");


    this.offerForm.patchValue({
      url: url + "-" + Date.now()
    });
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }
}
