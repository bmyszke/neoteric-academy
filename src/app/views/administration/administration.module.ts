import { NgModule } from '@angular/core';
import { AdministrationComponents } from './components';
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '../../material/material.module';
import { AdministrationServices } from './services';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';

// in this module we import every angular material module
@NgModule({
  imports: [
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    FlexLayoutModule
  ],
  declarations: [
    ...AdministrationComponents
  ],
  providers: [
    ...AdministrationServices
  ]
})
export class AdministrationModule { }
