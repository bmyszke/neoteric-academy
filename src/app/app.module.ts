import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material/material.module';
import { CoreGuards } from './guards';
import { SharedModule } from './shared/shared.module';
import { AuthModule } from './views/auth/auth.module';
import { OffersModule } from './views/offers/offers.module';
import { OfferResolver } from './views/offers/services/offer-resolver.service';
import { AdministrationModule } from './views/administration/administration.module';
import { AuthInterceptor } from './views/auth/auth.interceptor';
import { FetchOffersResolverService } from './views/offers/services/fetch-offers-resolver.service';
import { ErrorComponent } from './error/error.component';
import { ErrorInterceptor } from './error-interceptor';

@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    SharedModule,
    OffersModule,
    AuthModule,
    LeafletModule,
    FlexLayoutModule,
    AdministrationModule,
    HttpClientModule

  ],
  providers: [
    CoreGuards,
    OfferResolver,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    FetchOffersResolverService
  ],
  bootstrap: [AppComponent],
  entryComponents: [ErrorComponent]
})
export class AppModule { }
